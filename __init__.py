import re
import chanutils.torrent, urllib
from chanutils import get_doc, get_json, select_all, select_one, get_attr
from chanutils import get_text, get_text_content, replace_entity, byte_size
from chanutils import movie_title_year, series_season_episode
from playitem import TorrentPlayItem, PlayItemList

_SEARCH_URL = "https://www.skytorrents.in/search/all/ed/1/?l=en-us"

_FEEDLIST = [
  {'title':'Top', 'url':'https://www.skytorrents.in/top1000/all/ed/1?l=en-us'},
]

def name():
  return 'Sky Torrents'

def image():
  return 'icon.png'

def description():
  return "The Sky Torrents Channel (<a target='_blank' href='https://www.skytorrents.in'></a>)."

def feedlist():
  return _FEEDLIST

def feed(idx):
  doc = get_doc(_FEEDLIST[idx]['url'], proxy=False)
  return _extract(doc)

def search(q):
  url = _SEARCH_URL + "&q=" + urllib.quote(q.encode('utf8'))
  doc = get_doc(url, proxy=False)
  return _extract(doc)

def _is_video(title):
  title = title.lower()
  if 'photoshop' in title or 'microsoft' in title or 'adobe' in title or 'application' in title:
    return False
  if 'hdrip' in title or '720p' in title or '1080p' in title or 'yts.ag' in title or 'bdrip' in title or 'x264' in title or '.avi' in title or '.mkv' in title or '.mp4' in title or 'dvd' in title:
    return True
  match = re.match(r'.*([1-2][0-9]{3})', title)  # Match year
  if match is not None:
    return True
  return False

def _extract(doc):
  results = PlayItemList()
  rtree = select_all(doc, 'tr')
  first = True
  img = '/img/icons/film.svg'
  for l in rtree:
    if first:
      first = False
      continue
    td = select_one(l, 'td')
    el = select_one(td, 'a:nth-child(1)')
    title = get_text(el)
    if _is_video(title):
      el = select_one(td, 'a:last-child')
      url = get_attr(el, 'href')
      td = select_one(l, 'td:nth-child(2)')
      size = get_text(td)
      td = select_one(l, 'td:nth-child(5)')
      seeds = get_text(td)
      td = select_one(l, 'td:nth-child(6)')
      peers = get_text(td)
      subtitle = chanutils.torrent.subtitle(size, seeds, peers)
      subs = movie_title_year(title)
      results.add(TorrentPlayItem(title, img, url, subtitle, subs=subs))
  return results
